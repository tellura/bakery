var gulp = require('gulp'),
  sass = require('gulp-sass'),
  haml = require('gulp-haml'),
  template = require('gulp-template-html'),
  browserSync = require('browser-sync').create();

gulp.task('sass', function() {
  return gulp.src('app/scss/style.scss')
  .pipe(sass())
  .pipe(gulp.dest('dist/css'));
});

gulp.task('preHaml', function() {
  return gulp.src('app/layouts/layout.haml')
  .pipe(haml())
  .pipe(gulp.dest('app/layouts'));
});

gulp.task('image', function() {
  return gulp.src('app/images/**/*')
  .pipe(gulp.dest('dist/images'));
});

gulp.task('fonts', function() {
  return gulp.src('app/fonts/**/*')
  .pipe(gulp.dest('dist/fonts'));
});

gulp.task('haml', function() {
  return gulp.src('app/haml/**/*.haml')
  .pipe(haml())
  .pipe(template('app/layouts/layout.html'))
  .pipe(gulp.dest('dist'))
  .pipe(browserSync.reload({
      stream: true
    }));
});

gulp.task('default', function() {
  browserSync.init({
    server: {
      baseDir: 'dist'
    },
  });

  gulp.watch('app/**/*', ['sass', 'preHaml', 'image', 'fonts', 'haml']);
});
